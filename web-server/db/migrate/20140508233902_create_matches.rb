class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.string :game
      t.boolean :finished

      t.timestamps
    end
  end
end
