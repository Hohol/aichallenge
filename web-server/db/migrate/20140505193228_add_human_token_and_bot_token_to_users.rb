class AddHumanTokenAndBotTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :human_token, :string
    add_column :users, :bot_token, :string
  end
end
