class AddGameToRatings < ActiveRecord::Migration
  def change
    add_column :ratings, :game, :string
  end
end
