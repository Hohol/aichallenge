class AddPlayersToGame < ActiveRecord::Migration
  def change
    add_column :games, :player1_id, :integer
    add_column :games, :player2_id, :integer
  end
end
