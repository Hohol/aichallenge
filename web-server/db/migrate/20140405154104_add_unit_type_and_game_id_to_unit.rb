class AddUnitTypeAndGameIdToUnit < ActiveRecord::Migration
  def change
    add_column :units, :unit_type, :string
	add_column :units, :game_id, :integer
  end
end
