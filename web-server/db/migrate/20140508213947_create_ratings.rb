class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.decimal :value
      t.references :player

      t.timestamps
    end
    add_index :ratings, :player_id
  end
end
