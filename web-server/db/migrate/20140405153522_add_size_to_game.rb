class AddSizeToGame < ActiveRecord::Migration
  def change
    add_column :games, :size, :integer
  end
end
