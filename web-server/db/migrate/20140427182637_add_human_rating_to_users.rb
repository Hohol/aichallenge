class AddHumanRatingToUsers < ActiveRecord::Migration
  def change
    add_column :users, :human_rating, :decimal
  end
end
