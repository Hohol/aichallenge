class RemoveHumanRatingFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :human_rating
  end

  def down
    add_column :users, :human_rating, :decimal
  end
end
