class AddCoordinatesToUnit < ActiveRecord::Migration
  def change
    add_column :units, :x, :integer
    add_column :units, :y, :integer
  end
end
