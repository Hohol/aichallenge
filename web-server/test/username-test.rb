valid = [
	"ab",
	"AbAcaab",
	"a12345",
	"a_b",
	"__",
	"Hohol",
	"craus"
]

invalid = [
	"",
	"a",	
	"a b",
	"aba\ncaba",
	"aaaaa%k",
	"aaaaaaa.",
	"aaaaaa-a",
	".%",
	"\naaaa",
	"aaaaaaa\n",
	"\naaaaaa\n",
	" aaa",
	"aaa ",
	"  aaa "
]

def check(pattern, valid, invalid)
	valid.each do |x| 
	 	if not (pattern =~ x)
	 		p x
	 		return false
	 	end
	end
	invalid.each do |x| 
	 	if pattern =~ x
	 		p x
	 		return false
	 	end
	end
	return true
end

pattern = /\A[a-z0-9_][a-z0-9_]+\z/i
p check(pattern, valid, invalid)