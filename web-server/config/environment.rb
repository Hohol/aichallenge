# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
MazeServer::Application.initialize!

#Rails.configuration.faye_url = 'http://localhost:9292/bayeux'
Rails.configuration.server_ip = '188.226.213.37'
Rails.configuration.faye_url = "http://#{Rails.configuration.server_ip}:9292/bayeux"
Rails.configuration.socket_port = '9293'
