class Game < ActiveRecord::Base
  has_many :units, dependent: :destroy
  
  belongs_to :player1, class_name: 'User'
  belongs_to :player2, class_name: 'User'
  
  validates_numericality_of :size, 
    only_integer: true,
    greater_than_or_equal_to: 3,
    less_than_or_equal_to: 32,
    message: "can only be integer between 3 and 32."
end
