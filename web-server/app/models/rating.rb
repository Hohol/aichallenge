class Rating < ActiveRecord::Base
  belongs_to :player
  attr_accessible :value
end
