class Player < ActiveRecord::Base
  belongs_to :user
  attr_accessible :is_bot
end
