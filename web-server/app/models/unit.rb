class Unit < ActiveRecord::Base
  belongs_to :game
  symbolize :unit_type, in: [:neutral_wall, :player1, :player2, :wall1, :wall2, :gold], scopes: true
end
