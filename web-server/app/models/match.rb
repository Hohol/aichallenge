class Match < ActiveRecord::Base
  attr_accessible :finished, :game
  has_and_belongs_to_many :players
end
