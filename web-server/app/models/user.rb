require 'securerandom'

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  # attr_accessible :email, :password, :password_confirmation, :remember_me
  
  has_many :games_as_player1, foreign_key: :player1_id, class_name: 'Game'
  has_many :games_as_player2, foreign_key: :player2_id, class_name: 'Game'

  has_many :ratings, dependent: :destroy

  validates_uniqueness_of :username, :case_sensitive => false
  validates_format_of :username, :with => /\A[a-z0-9_][a-z0-9_]+\z/i
  #todo ensure case insensitiveness on db level

  before_create do
    self.bot_token = SecureRandom.hex
    self.human_token = SecureRandom.hex #should be valid faye channel name
  end

  after_create do
    bot = Player.new
    bot.is_bot = true
    bot.user_id = self.id
    bot.save

    human = Player.new
    human.is_bot = false
    human.user_id = self.id
    human.save
  end

  def games
    games_as_player1 + games_as_player2
  end
end
