function Game2048Manager(app) {
    this.mode = app.mode;
    this.app = app;
    this.inited = false;
}

Game2048Manager.prototype.processMessage = function(message) {
    console.log(JSON.stringify(message));
    if (message.type == 'init') {
        this.init(message); //todo =(
    } else if (message.type == 'newTile') {
        //noinspection JSUnresolvedVariable
        var tile = message.newTile;
        transpose(tile);
        this.addNewTile(tile);
    } else if (message.type == 'newTileAndMove' && !this.inited) {
        //noinspection JSUnresolvedVariable
        this.init(message.gameState);
    } else if(message.type == 'newTile' || message.type == 'newTileAndMove') {
        var invDirection = {up: 0, right: 1, down: 2, left: 3};
        this.move(invDirection[message.move]);
        //noinspection JSUnresolvedVariable
        transpose(message.newTile);
        //noinspection JSUnresolvedVariable
        this.addNewTile(message.newTile);
    }
};

Game2048Manager.prototype.init = function(gameState) {
    if(!this.inited) {
        this.inited = true;
        this.size = gameState.size; // Size of the grid
        this.inputManager = new KeyboardInputManager;
        this.storageManager = new LocalStorageManager; // todo remove
        this.actuator = new HTMLActuator;

        console.log(this.mode);

        if (this.mode == 'play') {
            this.inputManager.on("move", this.move.bind(this));
            this.inputManager.on("restart", this.restart.bind(this));
            this.inputManager.on("keepPlaying", this.keepPlaying.bind(this));
        } else if (this.mode == 'watch') {
            var label = document.getElementById("youAreWatching");
            //noinspection JSUnresolvedVariable
            label.innerHTML = 'You are watching match of ' + description(gameState.player);
        }
    }
    this.setup(gameState.board);
};

function transpose(tile) {
    var buf = tile.x;
    //noinspection JSSuspiciousNameCombination
    tile.x = tile.y;
    tile.y = buf;
}

// Restart the game
Game2048Manager.prototype.restart = function () {
    this.storageManager.clearGameState();
    this.actuator.continueGame(); // Clear the game won/lost message
    this.app.restart();
};

// Keep playing after winning (allows going over 2048)
Game2048Manager.prototype.keepPlaying = function () {
    this.keepPlaying = true;
    this.actuator.continueGame(); // Clear the game won/lost message
};

// Return true if the game is lost, or has won and the user hasn't kept playing
Game2048Manager.prototype.isGameTerminated = function () {
    return this.over || (this.won && !this.keepPlaying);
};

// Set up the game
Game2048Manager.prototype.setup = function (board) {
    var i;
    for (i = 0; i < board.length; i++) { //different axis in visualizer and server
        transpose(board[i]);
    }

    this.grid = new Grid(this.size);
    this.score = this.grid.getScore();
    this.over = false;
    this.won = false;
    this.keepPlaying = false;


    for (i = 0; i < board.length; i++) {
        this.grid.insertTile(new Tile({x: board[i].x, y: board[i].y}, board[i].value));
    }

    // Update the actuator
    this.actuate();
};

// Sends the updated grid to the actuator
Game2048Manager.prototype.actuate = function () {
    if (this.storageManager.getBestScore() < this.score) {
        this.storageManager.setBestScore(this.score);
    }

    // Clear the state when the game is over (game over only, not win)
    if (this.over) {
        this.storageManager.clearGameState();
    } else {
        this.storageManager.setGameState(this.serialize());
    }

    this.actuator.actuate(this.grid, {
        score: this.grid.getScore(),
        over: this.over,
        won: this.won,
        bestScore: this.storageManager.getBestScore(),
        terminated: this.isGameTerminated()
    });

};

Game2048Manager.prototype.actuateCell = function (cell) {
    if (this.storageManager.getBestScore() < this.score) {
        this.storageManager.setBestScore(this.score);
    }

    // Clear the state when the game is over (game over only, not win)
    if (this.over) {
        this.storageManager.clearGameState();
    } else {
        this.storageManager.setGameState(this.serialize());
    }

    this.actuator.actuateCell(cell, {
        score: this.grid.getScore(),
        over: this.over,
        won: this.won,
        bestScore: this.storageManager.getBestScore(),
        terminated: this.isGameTerminated()
    });

};

// Represent the current game as an object
Game2048Manager.prototype.serialize = function () {
    return {
        grid: this.grid.serialize(),
        score: this.score,
        over: this.over,
        won: this.won,
        keepPlaying: this.keepPlaying
    };
};

// Save all tile positions and remove merger info
Game2048Manager.prototype.prepareTiles = function () {
    this.grid.eachCell(function (x, y, tile) {
        if (tile) {
            tile.mergedFrom = null;
            tile.savePosition();
        }
    });
};

// Move a tile and its representation
Game2048Manager.prototype.moveTile = function (tile, cell) {
    this.grid.cells[tile.x][tile.y] = null;
    this.grid.cells[cell.x][cell.y] = tile;
    tile.updatePosition(cell);
};

// Move tiles on the grid in the specified direction
Game2048Manager.prototype.move = function (direction) {
    if (this.movementBlocked) {
        return;
    }
    // 0: up, 1: right, 2: down, 3: left
    var self = this;

    if (this.isGameTerminated()) return; // Don't do anything if the game's over

    var cell, tile;

    var vector = this.getVector(direction);
    var traversals = this.buildTraversals(vector);
    var moved = false;

    // Save the current tile positions and remove merger information
    this.prepareTiles();

    // Traverse the grid in the right direction and move tiles
    traversals.x.forEach(function (x) {
        traversals.y.forEach(function (y) {
            cell = { x: x, y: y };
            tile = self.grid.cellContent(cell);

            if (tile) {
                var positions = self.findFarthestPosition(cell, vector);
                var next = self.grid.cellContent(positions.next);

                // Only one merger per row traversal?
                if (next && next.value === tile.value && !next.mergedFrom) {
                    var merged = new Tile(positions.next, tile.value * 2);
                    merged.mergedFrom = [tile, next];

                    self.grid.insertTile(merged);
                    self.grid.removeTile(tile);

                    // Converge the two tiles' positions
                    tile.updatePosition(positions.next);
                } else {
                    self.moveTile(tile, positions.farthest);
                }

                if (!self.positionsEqual(cell, tile)) {
                    moved = true; // The tile moved from its original cell!
                }
            }
        });
    });

    if (moved) {
        this.blockMovement();
        this.actuate();
        if (this.mode == 'play') {
            var move = ['up', 'right', 'down', 'left'][direction];
            this.app.sendMove(move);
        }
        //this.move2();
    }
};

Game2048Manager.prototype.blockMovement = function () {
    this.movementBlocked = true;
};

Game2048Manager.prototype.unblockMovement = function () {
    this.movementBlocked = false;
};

Game2048Manager.prototype.addNewTile = function (tile) {
    this.score = this.grid.getScore();

    //this.grid.clearHistory();
    this.grid.insertTile(new Tile({x: tile.x, y: tile.y}, tile.value));

    if (!this.movesAvailable()) {
        this.over = true; // Game over!
    }

    this.actuateCell(tile);
    this.unblockMovement();
};

// Get the vector representing the chosen direction
Game2048Manager.prototype.getVector = function (direction) {
    // Vectors representing tile movement
    var map = {
        0: { x: 0, y: -1 }, // Up
        1: { x: 1, y: 0 },  // Right
        2: { x: 0, y: 1 },  // Down
        3: { x: -1, y: 0 }   // Left
    };

    return map[direction];
};

// Build a list of positions to traverse in the right order
Game2048Manager.prototype.buildTraversals = function (vector) {
    var traversals = { x: [], y: [] };

    for (var pos = 0; pos < this.size; pos++) {
        traversals.x.push(pos);
        traversals.y.push(pos);
    }

    // Always traverse from the farthest cell in the chosen direction
    if (vector.x === 1) traversals.x = traversals.x.reverse();
    if (vector.y === 1) traversals.y = traversals.y.reverse();

    return traversals;
};

Game2048Manager.prototype.findFarthestPosition = function (cell, vector) {
    var previous;

    // Progress towards the vector direction until an obstacle is found
    do {
        previous = cell;
        cell = { x: previous.x + vector.x, y: previous.y + vector.y };
    } while (this.grid.withinBounds(cell) &&
        this.grid.cellAvailable(cell));

    return {
        farthest: previous,
        next: cell // Used to check if a merge is required
    };
};

Game2048Manager.prototype.movesAvailable = function () {
    return this.grid.cellsAvailable() || this.tileMatchesAvailable();
};

// Check for available matches between tiles (more expensive check)
Game2048Manager.prototype.tileMatchesAvailable = function () {
    var self = this;

    var tile;

    for (var x = 0; x < this.size; x++) {
        for (var y = 0; y < this.size; y++) {
            tile = this.grid.cellContent({ x: x, y: y });

            if (tile) {
                for (var direction = 0; direction < 4; direction++) {
                    var vector = self.getVector(direction);
                    var cell = { x: x + vector.x, y: y + vector.y };

                    var other = self.grid.cellContent(cell);

                    if (other && other.value === tile.value) {
                        return true; // These two tiles can be merged
                    }
                }
            }
        }
    }

    return false;
};

Game2048Manager.prototype.positionsEqual = function (first, second) {
    return first.x === second.x && first.y === second.y;
};
