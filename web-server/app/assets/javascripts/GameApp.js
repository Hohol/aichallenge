function GameApp(GameManager, params) { //todo для разных режимов по идее разные классы напрашиваются
    this.mode = params.mode;
    //noinspection JSUnresolvedVariable
    this.client = new Faye.Client(params.faye_url);
    //noinspection JSUnresolvedVariable
    this.gameName = params.game_name;

    if (this.mode == 'play') {
        this.humanToken = params.human_token;
        //noinspection JSUnresolvedVariable
        this.playChannel = '/' + params.game_name;
        this.humanOnly = params.human_only; //todo it does not belong here
    } else if (this.mode == 'watch') {
        //noinspection JSUnresolvedVariable
        this.matchId = params.match_id;
        //noinspection JSUnresolvedVariable
        this.watcherToken = params.watcher_token;
    }

    this.gameManager = new GameManager(this);
}

//noinspection JSUnusedGlobalSymbols
GameApp.prototype.start = function () {
    if (this.mode == 'play') {
        this.play();
    } else if (this.mode == 'watch') {
        this.watch();
    }
};

GameApp.prototype.play = function () {
    var playChannel = '/' + this.humanToken;
    var me = this;
    this.client.subscribe(playChannel, function (message) {
        me.gameManager.processMessage(message);
    });

    this.sendGameRequest();

    setInterval(function () {
        me.send({human_token: me.humanToken, type: 'ping'});
    }, 1000);
};

GameApp.prototype.watch = function () { // ^_^
    var me = this;

    var inited = false;
    var watchChannel = '/watch-' + me.matchId;
    this.client.subscribe(watchChannel, function (message) {
        if (inited) {
            me.gameManager.processMessage(message);
        }
    });

    var requestChannel = '/' + this.watcherToken;
    this.client.subscribe(requestChannel, function (message) {
        if (message.type == 'init') {
            me.gameManager.processMessage(message);
            me.client.unsubscribe(requestChannel);
            inited = true;
        } else if(message.type == 'error') {
            //noinspection JSUnresolvedVariable
            alert(message.description);
        }
    });

    this.client.publish('/request-state', {
            watcherToken: this.watcherToken,
            game: this.gameName,
            matchId: this.matchId
        }
    );
};

function description(p) {
    return p.username + (p.type == 'bot' ? ' [bot]' : '');
}

GameApp.prototype.send = function (message) {
    if (message.type != 'ping') {
        console.log('sending');
        console.log(JSON.stringify(message));
        /**/
    }
    return this.client.publish(this.playChannel, message);
};

GameApp.prototype.sendMove = function (move) {
    console.log(this.humanToken);
    this.send({human_token: this.humanToken, data: {type: 'move', move: move}});
};

GameApp.prototype.sendGameRequest = function () {
    this.send({
        human_token: this.humanToken,
        data: {
            type: 'game-request',
            human_only: this.humanOnly //todo it does not belong here
        }
    });
};

GameApp.prototype.restart = function () {
    var publication = this.send({human_token: this.humanToken, data: {type: 'finish-match'}});
    var me = this;
    publication.then = function () {
        me.sendGameRequest();
    };
};