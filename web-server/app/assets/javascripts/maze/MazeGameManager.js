const walk = 'walk', placeWall = 'placeWall';
function MazeGameManager(app) {
    this.app = app;
    this.mode = app.mode;
    this.inited = false;
}

MazeGameManager.prototype.processMessage = function (message) {
    if (message.type == 'init') {
        this.init(message);
    } else if (message.type == 'move') {
        if (!this.inited) {
            //noinspection JSUnresolvedVariable
            this.init(message.gameState);
        } else {
            if (message.move instanceof String) { //todo get rid of this
                message.move = eval("(" + message.move + ")"); //TODO WTFIGO
            }
            /*console.log(JSON.stringify(message.move));
             console.log(typeof(message.move));/**/
            this.makeMove(message.move);
        }
    } else if (message.type == 'verdict') {
        //noinspection JSUnresolvedVariable
        if (message.data.description != '') {
            //noinspection JSUnresolvedVariable
            alert(message.data.description);
        }
    }
};

MazeGameManager.prototype.init = function (gameState) {
    this.inited = true;

    //noinspection JSUnresolvedVariable
    if (gameState.playerIndex == 0) {
        this.player = player1;
    } else { //noinspection JSUnresolvedVariable
        if (gameState.playerIndex == 1) {
            this.player = player2;
        }
    }
    this.board = new Board(gameState.size, gameState.board);
    this.stepsPerMove = gameState.stepsPerMove;
    //noinspection JSUnresolvedVariable
    this.score = {
        player1: gameState.player1Score,
        player2: gameState.player2Score
    };
    this.moveIndex = gameState.moveIndex;
    console.log('moveIndex = ' + this.moveIndex);
    this.remainingSteps = this.stepsPerMove;
    this.skipMoveCnt = gameState.skipMoveCnt;
    this.steps = [];
    this.visualizer = new Visualizer(gameState, this, this.mode);
};

MazeGameManager.prototype.goldOnBoardCnt = function () {
    var r = 0;
    for (var i = 0; i < this.board.n; i++) {
        for (var j = 0; j < this.board.n; j++) {
            if (this.board.cells[i][j] == gold) {
                r++;
            }
        }
    }
    return r;
};

MazeGameManager.prototype.makeMove = function (move) {
    var shouldSend = (this.whichMove() == this.player);
    var re = move;
    if (move == 'skip') {
        if (this.steps.length != 0) {
            re = this.steps;
        } else {
            this.skipMoveCnt++;
            re = 'skip';
        }
        this.updateStateAfterMove();
    } else if (move instanceof Array) {
        this.skipMoveCnt = 0;
        for (var i = 0; i < move.length; i++) {
            this.makeSimpleMove(move[i]);
        }
        this.updateStateAfterMove();
    } else {
        this.skipMoveCnt = 0;
        if (this.moveType() == walk) {
            this.makeSimpleMove(move);
            if (this.remainingSteps == 0 || this.gameFinished()) {
                re = this.steps;
                this.updateStateAfterMove();
            } else {
                re = 'partial';
            }
        } else {
            this.makeSimpleMove(move);
            this.updateStateAfterMove();
        }
    }
    if (shouldSend && re != 'partial') {
        this.app.sendMove(re);
    }
    this.onMoveEnded();
    if (this.gameFinished()) {
        this.visualizer.showVerdict(this.score);
    }
    return re;
};

MazeGameManager.prototype.gameFinished = function () {
    return this.allGoldCollected() || this.oneOfPlayersHasMoreThanHalfGold() || this.moveLimitExceeded() || this.tooManySkips();
};

MazeGameManager.prototype.allGoldCollected = function() {
    return this.goldOnBoardCnt() == 0;
};

MazeGameManager.prototype.oneOfPlayersHasMoreThanHalfGold = function () {
    var half = (this.goldOnBoardCnt() + this.score[player1] + this.score[player2]) / 2;
    return this.score[player1] > half || this.score[player2] > half;
};

MazeGameManager.prototype.moveLimitExceeded = function () {
    //todo implement
    return false;
};

MazeGameManager.prototype.tooManySkips = function () {
    return this.skipMoveCnt >= 5;
};

MazeGameManager.prototype.makeSimpleMove = function (position) {
    if (this.moveType() == walk) {
        var oldPosition = this.board.positionOf(this.whichMove());
        if (this.board.getCell(position) == gold) {
            this.score[this.whichMove()]++;
        }
        this.board.setCell(oldPosition, empty);
        this.board.setCell(position, this.whichMove());
        this.remainingSteps -= this.dist(oldPosition, position);
        this.steps.push(position);
    } else {
        if (this.whichMove() == player1) {
            this.board.setCell(position, wall1);
        } else {
            this.board.setCell(position, wall2);
        }
    }
};

MazeGameManager.prototype.updateStateAfterMove = function () {
    this.moveIndex++;
    this.steps = [];
    this.remainingSteps = this.stepsPerMove;
};

MazeGameManager.prototype.correctMove = function (position) {
    if (!this.inside(position.x, position.y)) {
        return false;
    }
    var value = this.board.getCell(position);
    if (this.moveType() == walk) {
        return (value == empty || value == gold) && this.mayWalkThisFar(position);
    } else {
        return value == empty && this.mayPlaceWall(position);
    }
};

MazeGameManager.prototype.mayWalkThisFar = function (to) {
    var from = this.board.positionOf(this.whichMove());
    return this.dist(from, to) <= this.remainingSteps;
};

MazeGameManager.prototype.dist = function (from, to) {
    var cells = this.getCellsWithStrictWallsForPlayer(this.whichMove());
    var dist = this.dfs(cells, from);
    return dist[to.x][to.y];
};

MazeGameManager.prototype.inside = function (x, y) {
    return x >= 0 && y >= 0 && x < this.board.n && y < this.board.n;
};

const dx = [1, -1, 0, 0];
const dy = [0, 0, -1, 1];

MazeGameManager.prototype.dfs = function (cells, start) { //it modifies cells!
    var q = [];
    cells[start.x][start.y] = 0;
    q.push(start);
    while (q.length > 0) {
        var p = q.shift();
        var x = p.x, y = p.y;
        for (var d = 0; d < 4; d++) {
            var toX = x + dx[d];
            var toY = y + dy[d];
            if (!this.inside(toX, toY)) {
                continue;
            }
            if (cells[toX][toY] != null) {
                continue;
            }
            cells[toX][toY] = cells[x][y] + 1;
            q.push({x: toX, y: toY});
        }
    }
    return cells;
};

MazeGameManager.prototype.mayPlaceWall = function (position) {
    var otherPlayer = this.whichMove() == player1 ? player2 : player1;
    var cells = this.getCellsWithStrictWallsForPlayer(otherPlayer);
    cells[position.x][position.y] = neutralWall;
    var otherPlayerPosition = this.board.positionOf(otherPlayer);
    var dist = this.dfs(cells, otherPlayerPosition);
    for (var i = 0; i < this.board.n; i++) {
        for (var j = 0; j < this.board.n; j++) {
            if (dist[i][j] == null) {
                return false;
            }
        }
    }
    return true;
};

MazeGameManager.prototype.getCellsWithStrictWallsForPlayer = function (target) {
    var cells = [];
    for (var i = 0; i < this.board.n; i++) {
        cells.push([]);
        for (var j = 0; j < this.board.n; j++) {
            var value = this.board.cells[i][j];
            if (value == neutralWall || target == player1 && value == wall2 ||
                target == player2 && value == wall1) {
                cells[i].push(neutralWall);
            } else {
                cells[i].push(null);
            }
        }
    }
    return cells;
};

MazeGameManager.prototype.expectedUnit = function () {
    var r = this.moveType() == walk ? 'player' : 'wall';
    r += this.whichMove() == player1 ? '1' : '2';
    return r;
};

MazeGameManager.prototype.whichMove = function () {
    var moveIndex = this.moveIndex;
    if (moveIndex >= 1) {
        moveIndex++;
    }
    moveIndex >>= 1;
    return [player1, player2][moveIndex % 2];
};

MazeGameManager.prototype.moveType = function () {
    var moveIndex = this.moveIndex;
    if (moveIndex >= 1) {
        moveIndex++;
    }
    return [walk, placeWall][moveIndex % 2];
};