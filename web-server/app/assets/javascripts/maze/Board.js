function Board(n, board) {
    this.n = n;
    this.cells = [];
    var i;
    for (i = 0; i < n; i++) {
        this.cells.push([]);
        for (var j = 0; j < n; j++) {
            this.cells[i].push(empty);
        }
    }

    for (i = 0; i < board.length; i++) {
        var cell = board[i];
        this.cells[cell.x][cell.y] = cell.value;
    }
}

Board.prototype.positionOf = function (unit) {
    for (var i = 0; i < this.n; i++) {
        for (var j = 0; j < this.n; j++) {
            if (this.cells[i][j] == unit) {
                return {x: i, y: j};
            }
        }
    }
};

Board.prototype.setCell = function (position, value) {
    this.cells[position.x][position.y] = value;
};

Board.prototype.getCell = function (position) {
    return this.cells[position.x][position.y];
};