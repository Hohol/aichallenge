const tileSize = 40;
const empty = 'empty', neutralWall = 'neutralWall', player1 = 'player1', player2 = 'player2',
    wall1 = 'wall1', wall2 = 'wall2', gold = 'gold';

var allUnits = [empty, neutralWall, player1, player2, wall1, wall2, gold];
imgs = {};

function Visualizer(gameState, gameManager, mode) {
    var me = this;
    var loadedCnt = 0;

    for (var i = 0; i < allUnits.length; i++) {
        var unit = allUnits[i];
        var img = new Image();
        img.src = '/assets/maze/' + unit + '.jpg';
        img.onload = function () {
            loadedCnt++;
            if (loadedCnt == allUnits.length) {
                draw();
            }
        };
        imgs[unit] = img;
    }

    function drawCell(i, j) {
        var img;
        if (i == hoverX && j == hoverY) {
            var name = gameManager.expectedUnit();
            img = imgs[name];
            ctx.globalAlpha = 0.4;
        } else {
            img = imgs[gameManager.board.cells[i][j]];
        }

        var x = i * (tileSize + 1) + 1;
        var y = j * (tileSize + 1) + 1;

        ctx.drawImage(img, x, y, tileSize - 1, tileSize - 1);
        ctx.globalAlpha = 1;
    }

    function draw() {
        if (loadedCnt != allUnits.length) {
            return;
        }
        var board = gameManager.board;

        var boardSize = (tileSize + 1) * board.n + 1;
        for (var x = 0; x < boardSize; x += tileSize + 1) {
            ctx.moveTo(x, 0);
            ctx.lineTo(x, boardSize);
            ctx.stroke();

            ctx.moveTo(0, x);
            ctx.lineTo(boardSize, x);
            ctx.stroke();
        }
        for (var i = 0; i < board.n; i++) {
            for (var j = 0; j < board.n; j++) {
                drawCell(i, j);
            }
        }
        var action = {
            walk: 'walk', placeWall: 'place wall'
        };
        curActionLabel.innerHTML = me.name[gameManager.whichMove()] + ' must ' + action[gameManager.moveType()] + ' now';
        if (gameManager.gameFinished()) {
            curActionLabel.style.visibility = 'hidden';
        }
        firstPlayerLabel.innerHTML = me.name[player1] + ' - ' + gameManager.score[player1];
        secondPlayerLabel.innerHTML = me.name[player2] + ' - ' + gameManager.score[player2];
        //scoreLabel.innerHTML = 'Score: ' + color[player1] + ', '
        //    + color[player2];
    }

    var curActionLabel = document.getElementById("currentActionLabel");
    var scoreLabel = document.getElementById("scoreLabel");
    var canvas = document.getElementById("mazeCanvas");

    var n = gameState.size;
    var boardSize = (tileSize + 1) * n + 1;

    canvas.width = boardSize;
    canvas.height = boardSize;
    var ctx = canvas.getContext('2d');

    var hoverX = -1;
    var hoverY = -1;

    draw();

    function getMouse(e) {
        var mouseX, mouseY;

        if (e.offsetX) {
            mouseX = e.offsetX;
            mouseY = e.offsetY;
        }
        else if (e.layerX) {
            mouseX = e.layerX;
            mouseY = e.layerY;
        }
        return {x: mouseX, y: mouseY};
    }

    if (mode == 'play') {
        canvas.onclick = function (e) {
            if (gameManager.whichMove() != gameManager.player) {
                return;
            }
            var p = getMouse(e);
            var x = Math.floor(p.x / (tileSize + 1));
            var y = Math.floor(p.y / (tileSize + 1));
            if (gameManager.correctMove({x: x, y: y})) {
                gameManager.makeMove({x: x, y: y});
            }
        };
    }

    if (mode == 'play') {
        canvas.onmousemove = function (e) {
            var p = getMouse(e);
            var x = Math.floor(p.x / (tileSize + 1));
            var y = Math.floor(p.y / (tileSize + 1));

            if (!gameManager.correctMove({x: x, y: y}) || gameManager.player != gameManager.whichMove()) {
                x = -1;
                y = -1;
            }

            if (x != hoverX || y != hoverY) {
                var oldHoverX = hoverX;
                var oldHoverY = hoverY;
                //noinspection JSUnusedAssignment
                hoverX = hoverY = -1;
                drawCell(oldHoverX, oldHoverY);
                hoverX = x;
                hoverY = y;
                drawCell(hoverX, hoverY);
            }
        };
    }

    canvas.onmouseout = function () {
        hoverX = -1;
        hoverY = -1;
        draw(gameManager);
    };

    var skipButton = document.getElementById("skipButton");
    var searchingLabel = document.getElementById("searchingLabel");
    var gameDiv = document.getElementById("gameDiv");
    gameDiv.style.visibility = 'visible';
    if (mode == 'play') {
        //skipButton.style.visibility = 'hidden';
        skipButton.style.display = 'inline';
    }
    searchingLabel.style.visibility = 'hidden';
    var firstPlayerLabel = document.getElementById("firstPlayer");
    var secondPlayerLabel = document.getElementById("secondPlayer");

    function description(p) {
        return p.username + (p.type == 'bot' ? ' [bot]' : '');
    }

    this.firstDescription = description(gameState.player1);
    this.secondDescription = description(gameState.player2);
    this.name = {player1: this.firstDescription, player2: this.secondDescription};

    skipButton.onclick = function () {
        if (gameManager.whichMove() != gameManager.player) {
            return;
        }
        gameManager.makeMove('skip');
    };

    gameManager.onMoveEnded = function () {
        hoverX = -1;
        hoverY = -1;
        draw();
    }
}

Visualizer.prototype.showVerdict = function (score) {
    var label = document.getElementById('verdict');
    if (score[player1] != score[player2]) {
        var winner = score[player1] > score[player2] ? this.name[player1] : this.name[player2];
        label.innerHTML = winner + ' won';
    } else {
        label.innerHTML = 'Draw';
    }
    label.style.display = 'inline';
};