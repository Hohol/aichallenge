class GamesController < ApplicationController
  def show
    @game_name = params[:id]
    @matches = Match.where(game: @game_name, finished: false)
  end

  def play
    unless current_user
      redirect_to new_user_session_path
      #redirect_to "users/sign_in"
      return
    end

    @game_name = params[:id]
    @mode = 'play'
    set_js_params

    @js_params[:human_only] = (params[:human_only] == '1')
    @js_params[:human_token] = current_user.human_token
    render params[:id]
  end

  def watch
    @game_name = params[:id]
    @mode = 'watch'
    set_js_params
    @js_params[:match_id] = params[:match_id]
    @js_params[:watcher_token] = 'w-' + SecureRandom.hex
    render params[:id]
  end

  def api
    @game_name = params[:id]
    @faye_url =
    render "#{@game_name}-api"
  end

  def set_js_params
    @js_params = {
        faye_url: Rails.configuration.faye_url,
        mode: @mode,
        game_name: @game_name
    }
  end
end
