initBot = function(gameManager) {
  var inf = 12345
  var board = gameManager.board
  var n = board.n
  
  var placeWall = function() {
    x = rnd(0, n)
    y = rnd(0, n)
    clickCell(x,y)
  }
  
  var move = function() {
    if (gameManager.moveType == 'walk') {
      walk()
    } else {
      placeWall()
    }
  }  
  
  var newMap = function() {
    var x = new Array(n)
    for (var i = 0; i < n; i++) {
      x[i] = new Array(n)
      for (var j = 0; j < n; j++) {
        x[i][j] = {moves: inf, ap: inf}
      }
    }
    return x
  }
  
  var distancesFrom = function(point) {
    a = newMap()
    a[point.x][point.y].moves = 0
    a[point.x][point.y].ap = 0
  }
  
  var walk = function() {
    var distanceMap = distancesFrom(gameManager.board.positionOf(player))
    x = rnd(0, n)
    y = rnd(0, n)
    clickCell(x,y)
  }
  
  var think = function() {
    if (gameManager.whichMove == player) {
      move()
    }
  }
  
  setInterval(think, 10)
}