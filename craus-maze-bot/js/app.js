const tileSize = 40;
const empty = 'empty', neutralWall = 'neutralWall', player1 = 'player1', player2 = 'player2',
wall1 = 'wall1', wall2 = 'wall2', gold = 'gold';

var	allUnits = [empty, neutralWall, player1, player2, wall1, wall2, gold];
imgs = {};
var loadedCnt = 0;
for (i in allUnits) {
	var unit = allUnits[i];
	var img = new Image();
	img.src = 'img/' + unit + '.jpg';
	imgs[unit] = img;
	/*img.onload = function() {
		loadedCnt++;
		if(loadedCnt == allUnits.length) {
			doEverything();
		}
	}/**/
}

var curActionLabel = document.getElementById("currentActionLabel");
var scoreLabel = document.getElementById("scoreLabel");
var canvas = document.getElementById("mazeCanvas");

function drawCell(i,j) {
	if(i == hoverX && j == hoverY) {
		var name = gameManager.expectedUnit();    				
		var img = imgs[name];    				
		ctx.globalAlpha = 0.4;
	} else {
		var img = imgs[gameManager.board.cells[i][j]];
	}
	
	var x = i*(tileSize+1)+1;
	var y = j*(tileSize+1)+1;
	
	ctx.drawImage(img, x, y, tileSize-1, tileSize-1);
	ctx.globalAlpha = 1;
}

function draw() {
	var board = gameManager.board;
	var boardSize = (tileSize+1)*board.n + 1;
    for(var i = 0; i < boardSize; i += tileSize+1) {
    	ctx.moveTo(i, 0);
    	ctx.lineTo(i, boardSize);
    	ctx.stroke();

    	ctx.moveTo(0, i);
    	ctx.lineTo(boardSize, i);
    	ctx.stroke();
    }
    for (var i = 0; i < board.n; i++) {
    	for (var j = 0; j < board.n; j++) { 
    		drawCell(i,j);
    	}
    }
    curActionLabel.innerHTML = gameManager.whichMove + ' must ' + gameManager.moveType + ' now';
    scoreLabel.innerHTML = player1 + ': ' + gameManager.score[player1] + '; ' + player2 + ': ' + gameManager.score[player2];
}

clickCellBlocked = false
function clickCell(x,y) {
  if (clickCellBlocked) return;
  if(gameManager.correctMove({x:x, y:y})) {
    var r = gameManager.makeMove({x:x,y:y});
    if(r != 'partial') {
      clickCellBlocked = true;
      sendMove(r).then(function() { clickCellBlocked = false; bot.think(); })
    } 
  } 
}

function initBoard(gameState) {

	var n = gameState.size;
	var boardSize = (tileSize+1)*n + 1;
	
	canvas.width = boardSize;
	canvas.height = boardSize;
	ctx = canvas.getContext('2d');
	
	hoverX = -1;
    hoverY = -1;

	gameManager = new GameManager(gameState);	

	draw();

	canvas.onclick = function(e) {
		if(gameManager.whichMove != player) {
			return;
		}
    var x = Math.floor((e.pageX - canvas.offsetLeft) / (tileSize+1));
    var y = Math.floor((e.pageY - canvas.offsetTop)  / (tileSize+1));
    clickCell(x,y);
  };

  canvas.onmousemove = function(e) {
    var x = Math.floor((e.pageX - canvas.offsetLeft) / (tileSize+1));
    var y = Math.floor((e.pageY - canvas.offsetTop)  / (tileSize+1));

    if(!gameManager.correctMove({x:x,y:y}) || player != gameManager.whichMove) {
      x = -1;
      y = -1;
    }

    if(x != hoverX || y != hoverY) {
      var oldHoverX = hoverX;
      var oldHoverY = hoverY;
      hoverX = hoverY = -1;
      drawCell(oldHoverX, oldHoverY);
      hoverX = x;
      hoverY = y;
      drawCell(hoverX, hoverY);
    }
  }

  canvas.onmouseout = function(e) {
    hoverX = -1;
    hoverY = -1;
    draw(gameManager);
  }

	var skipButton = document.getElementById("skipButton");
	skipMove = skipButton.onclick = function() {
		if(gameManager.whichMove != player) {
			return instantCallback;
		}
		if(gameManager.steps.length != 0) {
      var move = gameManager.steps;
      var result = sendMove(move);
		} else {
			var result = sendMove('skip');
		}
    gameManager.makeMove('skip');   
    return result;
	}

	gameManager.onMoveEnded = function() {
		hoverX = -1;
		hoverY = -1;
		draw();
	}
}


