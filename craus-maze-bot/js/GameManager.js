const walk = 'walk', placeWall = 'placeWall';
function GameManager(gameState) {
	this.board = new Board(gameState.size, gameState.board);
	this.stepsPerMove = gameState.stepsPerMove;
	this.whichMove = player1;
	this.moveType = 'walk';
	this.score = {player1 : 0, player2: 0};
	this.moveIndex = 0;
	this.remainingSteps = this.stepsPerMove;
	this.steps = [];
  initBot(this)
}

GameManager.prototype.makeMove = function(move) {
	var re = move;
	if(move == 'skip') {
		this.updateStateAfterMove();
	} else if(move instanceof Array) {
		for(var i = 0; i < move.length; i++) {
			this.makeSimpleMove(move[i]);
		}
		this.updateStateAfterMove();
	} else {
		if(this.moveType == walk) {
			this.makeSimpleMove(move);
			if(this.remainingSteps == 0) {
				var re = this.steps;
				this.updateStateAfterMove();
			} else {
				re = 'partial';
			}
		} else {
			this.makeSimpleMove(move);
			this.updateStateAfterMove();
		}
	}
	this.onMoveEnded();
	return re;
}

GameManager.prototype.makeSimpleMove = function(position) {
	if(this.moveType == walk) {
		var oldPosition = this.board.positionOf(this.whichMove);
		if(this.board.getCell(position) == gold) {
			this.score[this.whichMove]++;
		}
		this.board.setCell(oldPosition, empty);
		this.board.setCell(position, this.whichMove);
		this.remainingSteps -= this.dist(oldPosition, position);
		this.steps.push(position);
	} else {
		if (this.whichMove == player1) {
			this.board.setCell(position, wall1);
		} else {
			this.board.setCell(position, wall2);
		}
	}
}

GameManager.prototype.updateStateAfterMove = function() {
	this.steps = [];
	if(this.moveType == walk) {
		this.moveType = placeWall;
	} else {
		this.whichMove = this.whichMove == player1 ? player2 : player1;
		this.moveType = walk;
		this.remainingSteps = this.stepsPerMove;
	}
	this.moveIndex++;
	if(this.moveIndex == 1) {
		this.updateStateAfterMove();
	}
	hoverX = hoverY = -1;
}

GameManager.prototype.correctMove = function(position) {
	if(!this.inside(position.x, position.y)) {
		return false;
	}
	var value = this.board.getCell(position);
	if(this.moveType == walk) {
		return (value == empty || value == gold) && this.mayWalkThisFar(position);
	} else {
		return value == empty && this.mayPlaceWall(position);
	}
}

GameManager.prototype.mayWalkThisFar = function(to) {
	var from = this.board.positionOf(this.whichMove);
	return this.dist(from, to) <= this.remainingSteps;
}

GameManager.prototype.dist = function(from, to) {
	var cells = this.getCellsWithStrictWallsForPlayer(this.whichMove);
	var dist = this.dfs(cells, from);
	return dist[to.x][to.y];
}

GameManager.prototype.inside = function(x, y) {
	return x >= 0 && y >= 0 && x < this.board.n && y < this.board.n;
}

const dx = [1, -1, 0, 0];
const dy = [0, 0, -1, 1]

GameManager.prototype.dfs = function(cells, start) { //it modifies cells!
	var q = [];
	cells[start.x][start.y] = 0;
	q.push(start);
	while(q.length > 0) {
		var p = q.shift();
		var x = p.x, y = p.y;
		for(var d = 0; d < 4; d++) {
			var toX = x+dx[d];
			var toY = y+dy[d];
			if(!this.inside(toX, toY)) {
				continue;
			}
			if(cells[toX][toY] != null) {
				continue;
			}
			cells[toX][toY] = cells[x][y]+1;
			q.push({x:toX, y:toY});
		}
	}
	return cells;
}

GameManager.prototype.mayPlaceWall = function(position) {
	var otherPlayer = this.whichMove == player1 ? player2 : player1;
	var cells = this.getCellsWithStrictWallsForPlayer(otherPlayer);
	cells[position.x][position.y] = neutralWall;
	var otherPlayerPosition = this.board.positionOf(otherPlayer);
	var dist = this.dfs(cells, otherPlayerPosition);
	for(var i = 0; i < this.board.n; i++) {
		for(var j = 0; j < this.board.n; j++) {
			if(dist[i][j] == null) {
				return false;
			}
		}
	}
	return true;
}

GameManager.prototype.getCellsWithStrictWallsForPlayer = function(player) {
	var cells = [];
	for (var i = 0; i < this.board.n; i++) {
		cells.push([]);
		for (var j = 0; j < this.board.n; j++) {
			var value = this.board.cells[i][j];
			if(value == neutralWall || player == player1 && value == wall2 ||
									   player == player2 && value == wall1) {
				cells[i].push(neutralWall);
			} else {
				cells[i].push(null);
			}
		}
	}
	return cells;
}

GameManager.prototype.expectedUnit = function() {
	var r = this.moveType == walk ? 'player' : 'wall';
	r += this.whichMove == player1 ? '1' : '2';
	return r;
}