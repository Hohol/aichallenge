//var client = new Faye.Client("http://localhost:9292/bayeux");
var client = new Faye.Client("http://192.168.0.101:9292/bayeux");
var user = 'crausBot' + Math.floor(Math.random()*100000);
var serverChannel = '/maze';

client.subscribe('/' + user, function(message) {
	console.log('received');
	console.log(stringify(message));
	
	if(message.type == 'init') {
		var gameState = message; //todo use property
		//gameState.board = eval(gameState.board); //todo get rid of eval
		if(gameState.playerIndex == 0) {
			player = player1;
		} else {
			player = player2;
		}
		initBoard(gameState);
	} else if(message.type == 'move') {
		if(message.move instanceof String) { //todo get rid of this
			message.move = eval("(" + message.move + ")");
		}
		console.log(stringify(message.move));
		console.log(typeof(message.move));
		gameManager.makeMove(message.move);
	}
});

client.publish(serverChannel, {user:user, type:'game-request'});

function send(message) {
	console.log('sending');
	console.log(JSON.stringify(message));
	return client.publish(serverChannel, message);
}

function sendMove(move) {
	return send({user: user, type:'move', move:move});
}

function stringify(x) {
	return JSON.stringify(x);//.replace(/\"([^(\")"]+)\":/g,"$1:");
}