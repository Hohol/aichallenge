package games.maze;

import static games.maze.MazeGameState.N;

import static games.maze.Unit.*;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.*;

@Test
public class MoveValidatorTest {
    MoveValidator validator = new MoveValidator();
    Unit[][] board;

    @BeforeMethod
    void init() {
        board = new Unit[N][N];
        for (int i = 0; i < N; i++) {
            Arrays.fill(board[i], empty);
        }
    }


    @Test
    void testWalk() {
        board[0][0] = player1;
        check(new int[][] {
                {0, 1, 1, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0, 0, 0},
                {1, 1, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
        },
                3        
        );

        board[0][1] = player2;

        check(new int[][] {
                {0, 0, 1, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0, 0, 0},
                {1, 1, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
        },
                3
        );

        board[1][0] = neutralWall;

        check(new int[][] {
                {0, 0, 1, 1, 0, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
        },
                3
        );
    }

    @Test
    void testWall() {
        board[0][1] = player1;
        board[1][7] = wall1;
        board[3][7] = wall1;
        board[4][6] = wall2;
        board[6][6] = neutralWall;
        board[7][6] = neutralWall;
        board[7][1] = wall2;
        board[7][4] = player2;


        checkWall(new int[][]{
                {1, 0, 1, 1, 1, 1, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 0, 0},
                {1, 0, 1, 1, 0, 1, 0, 1},
        });
    }

    @Test
    void testSamePositionBug() {
        board[0][0] = player1;
        assertTrue(validator.isValidMove(board, player1,
                moves(new Position(0,1), new Position(0,0)),3
                ));
    }

    private void checkWall(int[][] expected) {
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[i].length; j++) {
                boolean b = expected[i][j] == 1;
                assertEquals(validator.mayPlaceWall(board, player1, new Position(i,j)), b, i + " " + j);
            }
        }
    }

    private void check(int[][] expected, int steps) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                boolean b = expected[i][j] == 1;
                assertEquals(validator.isValidMove(board, player1, moves(new Position(i,j)), steps), b);
            }
        }
    }

    private List<Position> moves(Position... moves) {
        return Arrays.asList(moves);
    }

}
