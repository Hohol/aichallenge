package games.maze;

import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class MazeGameStateTest {
    @Test
    void testWhoMovesNow() {
        Assert.assertEquals(MazeGameState.whoMovesNow(0), 0);
        Assert.assertEquals(MazeGameState.whoMovesNow(1), 1);
        Assert.assertEquals(MazeGameState.whoMovesNow(2), 1);
        Assert.assertEquals(MazeGameState.whoMovesNow(3), 0);
        Assert.assertEquals(MazeGameState.whoMovesNow(4), 0);
        Assert.assertEquals(MazeGameState.whoMovesNow(5), 1);
        Assert.assertEquals(MazeGameState.whoMovesNow(6), 1);
        Assert.assertEquals(MazeGameState.whoMovesNow(7), 0);
    }
}
