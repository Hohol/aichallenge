package server.common;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

@Test
public class MatchRequestQueueTest {
    @Test
    void test() {
        MatchRequestQueue q = new MatchRequestQueue();
        Player hohol = new Player("Hohol", PlayerType.human, "", 0);
        MatchRequest hoholRequest = new MatchRequest(hohol, true);

        Player bot = new Player("Bot", PlayerType.bot, "", 1);
        MatchRequest botRequest = new MatchRequest(bot, false);

        q.add(botRequest);
        assertEquals(q.findOpponentFor(hoholRequest), null);
        q.add(hoholRequest);
        q.remove(bot);
        assertEquals(q.findOpponentFor(botRequest), null);

        Player human2 = new Player("Human2", PlayerType.human, "", 2);
        MatchRequest human2Request = new MatchRequest(human2, false);
        assertEquals(q.findOpponentFor(human2Request), hohol);
    }
}
