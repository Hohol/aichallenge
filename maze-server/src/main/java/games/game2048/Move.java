package games.game2048;

public enum Move {

    left(0, -1, "<"),
    up(-1, 0, "^"),
    right(0, 1, ">"),
    down(1, 0, "v");

    public final int dx, dy;
    public final String arrow;
    public static final Move[] ALL = Move.values();

    private Move(int dx, int dy, String arrow) {
        this.dx = dx;
        this.dy = dy;
        this.arrow = arrow;
    }
}

