package games.game2048;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import server.common.AbstractMatch;
import server.common.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameState extends AbstractMatch {
    public static final int N = 4;

    int[][] board = new int[N][N];
    Random rnd = new Random();
    private final Player player;

    GameState(long matchId, Player player) {
        super(matchId);
        this.player = player;
        addRandomTile();
        addRandomTile();
    }

    public Tile addRandomTile() {
        List<Integer> xList = new ArrayList<>();
        List<Integer> yList = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (board[i][j] == 0) {
                    xList.add(i);
                    yList.add(j);
                }
            }
        }
        if (xList.isEmpty()) {
            return null;
        }
        int index = rnd.nextInt(xList.size());
        int value = rnd.nextDouble() < 0.1 ? 4 : 2;
        Integer x = xList.get(index);
        Integer y = yList.get(index);
        board[x][y] = value;
        return new Tile(x, y, value);
    }

    public boolean makeMove(Move move) {
        int[][] newBoard = makeMove(board, move);
        if(same(board, newBoard)) {
            return false;
        }
        board = newBoard;
        return true;
    }

    private static boolean same(int[][] a, int[][] b) {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if(a[i][j] != b[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    private static int[][] makeMove(int[][] board, Move move) {
        int n = board.length;
        int m = board[0].length;
        int[][] r = new int[n][];
        for (int i = 0; i < n; i++) {
            r[i] = board[i].clone();
        }
        if (move.dx == 1 || move.dy == 1) {
            for (int i = n - 1; i >= 0; i--) {
                for (int j = m - 1; j >= 0; j--) {
                    move(r, i, j, move.dx, move.dy, n, m);
                }
            }
        } else {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    move(r, i, j, move.dx, move.dy, n, m);
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (r[i][j] < 0) {
                    r[i][j] *= -1;
                }
            }
        }
        return r;
    }


    static void move(int[][] board, int x, int y, int dx, int dy, int n, int m) {
        while (true) {
            if (board[x][y] == 0) {
                break;
            }
            int tox = x + dx;
            int toy = y + dy;
            if (!inside(tox, toy, n, m)) {
                break;
            }
            if (board[tox][toy] == 0) {
                board[tox][toy] = board[x][y];
                board[x][y] = 0;
                x = tox;
                y = toy;
            } else if (board[tox][toy] == board[x][y]) {
                board[tox][toy] = -board[x][y] * 2;
                board[x][y] = 0;
                break;
            } else {
                break;
            }
        }
    }

    public static boolean inside(int x, int y, int n, int m) {
        return x >= 0 && y >= 0 && x < n && y < m;
    }

    public boolean finished() {
        for (Move move : Move.ALL) {
            int[][] newBoard = makeMove(board, move);
            if(!same(board, newBoard)) {
                return false;
            }
        }
        return true;
    }
    public void print() {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                System.out.print(board[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public long getScore() {
        long r = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                r += board[i][j];
            }
        }
        return r;
    }

    @Override
    public JsonObject getInitMessage(Player recipient) {
        JsonObject data = new JsonObject();
        data.addProperty("type", "init");
        data.add("board", getTiles());
        data.addProperty("size", GameState.N);
        if (recipient == null) {
            data.add("player", playerDescription(player));
        }
        return data;
    }

    JsonElement getTiles() {
        List<Tile> r = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (board[i][j] != 0) {
                    r.add(new Tile(i, j, board[i][j]));
                }
            }
        }
        return gson.toJsonTree(r);
    }
}
