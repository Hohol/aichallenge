package games.game2048;

import com.google.gson.JsonObject;
import server.common.*;

public class Game2048Manager extends AbstractGameManager <GameState> {

    @Override
    public String getGameName() {
        return "2048";
    }

    @Override
    synchronized public void processMessage(Player player, String message, Endpoint endpoint) {
        JsonObject data = gson.fromJson(message, JsonObject.class);
        try {
            userToEndpoint.put(player, endpoint);
            String type = data.get("type").getAsString();
            switch (type) {
                case "game-request":
                    startMatch(player);
                    break;
                case "finish-match":
                    finishMatch(player);
                    break;
                case "move":
                    makeMove(player, data);
                    break;
                case "ping":
                    //do nothing
                    break;
                default:
                    System.out.println("Unknown message type");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(message);
        }
    }

    private void makeMove(Player player, JsonObject data) {
        Move move = gson.fromJson(data.get("move"), Move.class);
        GameState gameState = activeGames.get(player);
        if(gameState.makeMove(move)) {
            Tile tile = gameState.addRandomTile();
            gameState.print();
            sendNewTile(player, tile, gameState);
            broadcast(gameState, tile, move);
            if(gameState.finished()) {
                finishMatch(player);
            }
        } else {
            sendError(player, "Incorrect move");
        }
    }

    private void finishMatch(Player player) {
        stopListening(player);
        GameState gameState = activeGames.get(player);
        if(gameState != null) {
            activeGames.remove(player);
            updateRating(player, gameState);
            DBConnector.getInstance().finishMatch(gameState.getMatchId());
        }
    }

    private void stopListening(Player player) {
        getEndpoint(player).stopListening(player, getGameName());
    }

    private void updateRating(Player player, GameState gameState) {
        DBConnector dbConnector = DBConnector.getInstance();
        Number oldRating = dbConnector.getRating(getGameName(), player.getId());
        boolean hasOldRating = oldRating != null;
        long score = gameState.getScore();
        if(!hasOldRating || score > oldRating.doubleValue()) {
            dbConnector.updateRating(getGameName(), player.getId(), hasOldRating, score);
        }
    }

    private void sendNewTile(Player player, Tile tile, GameState gameState) {
        JsonObject data = new JsonObject();
        data.addProperty("type", "newTile");
        data.add("newTile", gson.toJsonTree(tile));
        data.add("board", gameState.getTiles());
        send(player, data);
    }

    private void broadcast(GameState gameState, Tile tile, Move move) {
        JsonObject data = new JsonObject();
        data.addProperty("type", "newTileAndMove");
        data.add("newTile", gson.toJsonTree(tile));
        data.add("move", gson.toJsonTree(move));
        data.add("gameState", gameState.getInitMessage(null));
        broadcaster.sendToWatchers(gameState.getMatchId(), data);
    }

    private void startMatch(Player player) {
        GameState gameState = activeGames.get(player);
        if(gameState == null) {
            long matchId = DBConnector.getInstance().startMatch(getGameName(), player.getId());
            gameState = new GameState(matchId, player);
            activeGames.put(player, gameState);
        }
        gameState.print();
        sendInitialGameState(player, gameState);
    }

    @Override
    synchronized public void playerDisconnected(Player player) {
        Endpoint endpoint = userToEndpoint.get(player);
        if(endpoint == null) {
            return;
        }
        finishMatch(player);
    }
}
