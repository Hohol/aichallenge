package games.maze;

import java.util.Arrays;
import java.util.Random;

import static games.maze.Unit.*;
import static games.maze.Unit.player1;
import static games.maze.Unit.player2;
import static java.lang.Math.abs;
import static java.lang.Math.min;

public class DefaultBoardGenerator implements BoardGenerator {

    private Random rnd = new Random();
    private final int N, goldCnt;

    public DefaultBoardGenerator(int n, int goldCnt) {
        N = n;
        this.goldCnt = goldCnt;
    }

    public Unit[][] generateBoard() {
        Unit[][] board = new Unit[N][N];
        for (int i = 0; i < N; i++) {
            Arrays.fill(board[i], empty);
        }

        for (int i = 0; i < 100; i++) {
            int x = rnd.nextInt(N - 2) + 1;
            int y = rnd.nextInt(N - 2) + 1;
            if (areaFreeFrom(board, x, y, neutralWall)) {
                board[x][y] = neutralWall;
            }
        }

        for (int i = 0; i < goldCnt; i++) {
            placeGold(board);
        }

        placePlayers(board);
        return board;
    }

    private int distToGold(Unit[][] board, int x, int y) {
        int mi = Integer.MAX_VALUE;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (board[i][j] == gold) {
                    mi = min(mi, abs(x - i) + abs(y - j));
                }
            }
        }
        return mi;
    }

    private void placeGold(Unit[][] board) {
        while (true) {
            int x, y;
            if (rnd.nextBoolean()) {
                if (rnd.nextBoolean()) {
                    x = 0;
                } else {
                    x = N - 1;
                }
                y = rnd.nextInt(N);
            } else {
                if (rnd.nextBoolean()) {
                    y = 0;
                } else {
                    y = N - 1;
                }
                x = rnd.nextInt(N);
            }
            if (areaFreeFrom(board, x, y, gold)) {
                board[x][y] = gold;
                return;
            }
        }
    }

    private boolean areaFreeFrom(Unit[][] board, int x, int y, Unit unit) {
        for (int dx = -1; dx <= 1; dx++) {
            for (int dy = -1; dy <= 1; dy++) {
                if (!inside(x + dx, y + dy)) {
                    continue;
                }
                if (board[x + dx][y + dy] == unit) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean inside(int x, int y) {
        return x >= 0 && x < N && y >= 0 && y < N;
    }

    private void placePlayers(Unit[][] board) {
        int ma = -1;
        int bestX = -1, bestY = -1;
        int bestX2 = -1, bestY2 = -1;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (j < N - 1 && board[i][j] == empty && board[i][j + 1] == empty) {
                    int dist = min(distToGold(board, i, j), distToGold(board, i, j + 1));
                    if (dist > ma) {
                        ma = dist;
                        bestX = i;
                        bestY = j;
                        bestX2 = i;
                        bestY2 = j + 1;
                    }
                }
                if (i < N - 1 && board[i][j] == empty && board[i + 1][j] == empty) {
                    int dist = min(distToGold(board, i, j), distToGold(board, i + 1, j));
                    if (dist > ma) {
                        ma = dist;
                        bestX = i;
                        bestY = j;
                        bestX2 = i + 1;
                        bestY2 = j;
                    }
                }
            }
        }
        if (rnd.nextBoolean()) {
            board[bestX][bestY] = player1;
            board[bestX2][bestY2] = player2;
        } else {
            board[bestX][bestY] = player2;
            board[bestX2][bestY2] = player1;
        }
    }
}
