package games.maze;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import server.common.AbstractMatch;
import server.common.MatchResult;
import server.common.Player;

import java.util.*;

import static games.maze.Unit.*;

public class MazeGameState extends AbstractMatch {

    public static final int N = 8;
    public static final int GOLD_CNT = 5;
    public static final int STEPS_PER_MOVE = 3;
    private static final int GAME_LENGTH_LIMIT = 100;
    private static final int WALK = 0;
    private static final int PLACE_WALL = 1;
    private final int playerCnt = 2;

    private final List<Player> players = new ArrayList<>();
    private final Unit[][] board;
    private final int[] score = new int[playerCnt];
    private int moveIndex;
    private int skipMoveCnt;
    private static final MoveValidator validator = new MoveValidator();

    public MazeGameState(Player firstPlayer, Player secondPlayer, long matchId) {
        super(matchId);
        players.add(firstPlayer);
        players.add(secondPlayer);
        BoardGenerator boardGenerator = new DefaultBoardGenerator(N, GOLD_CNT);
        board = boardGenerator.generateBoard();
    }

    public Player whoMovesNow() {
        return players.get(whoMovesNow(moveIndex));
    }

    private Unit whatUnitMovesNow() {
        if (whoMovesNow(moveIndex) == 0) {
            return player1;
        } else {
            return player2;
        }
    }

    static int whoMovesNow(int moveIndex) {
        if (moveIndex >= 1) {
            moveIndex++;
        }
        moveIndex /= 2;
        return moveIndex % 2;
    }

    public Player whoNotMovesNow() {
        return players.get(1 - whoMovesNow(moveIndex));
    }

    public void skipMove() {
        skipMoveCnt++;
        moveIndex++;
    }

    private void makeSimpleMove(Position pos) {
        skipMoveCnt = 0;
        Unit who = whatUnitMovesNow();
        clearCell(who);
        if (board[pos.x][pos.y] == gold) {
            score[whoMovesNow(moveIndex)]++;
        }
        board[pos.x][pos.y] = who;
    }

    public void placeWall(Position pos) {
        skipMoveCnt = 0;
        if (whatUnitMovesNow() == player1) {
            board[pos.x][pos.y] = wall1;
        } else {
            board[pos.x][pos.y] = wall2;
        }
        moveIndex++;
    }

    Position positionOf(Unit unit) {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (board[i][j] == unit) {
                    return new Position(i, j);
                }
            }
        }
        throw new RuntimeException();
    }

    private void clearCell(Unit who) {
        Position pos = positionOf(who);
        board[pos.x][pos.y] = empty;
    }

    private int whatMoveType(int moveIndex) {
        if (moveIndex >= 1) {
            moveIndex++;
        }
        return moveIndex % 2;
    }/**/

    public void makeMove(List<Position> steps) {
        for (Position step : steps) {
            makeSimpleMove(step);
        }
        moveIndex++;
    }

    public boolean validMove(List<Position> moves) {
        if (whatMoveType(moveIndex) != WALK) {
            return false;
        }
        return validator.isValidMove(board, whatUnitMovesNow(), moves, STEPS_PER_MOVE);
    }

    public boolean mayPlaceWall(Position to) {
        if (whatMoveType(moveIndex) != PLACE_WALL) {
            return false;
        }
        return validator.mayPlaceWall(board, whatUnitMovesNow(), to);
    }

    public void print() {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                System.out.print(board[j][i].getChar());
            }
            System.out.println();
        }
        System.out.println("moveIndex = " + moveIndex);
        System.out.println();
    }

    public boolean finished() {
        return allGoldCollected() || oneOfPlayersHasMoreThanHalfGold() || moveLimitExceeded() || tooManySkips();
    }

    private boolean tooManySkips() {
        return skipMoveCnt >= 5;
    }

    private boolean allGoldCollected() {
        return score[0] + score[1] == GOLD_CNT;
    }

    private boolean oneOfPlayersHasMoreThanHalfGold() {
        int half = GOLD_CNT / 2;
        return score[0] > half || score[1] > half;
    }

    private boolean moveLimitExceeded() {
        return moveIndex == GAME_LENGTH_LIMIT;
    }

    public Player getFirstPlayer() {
        return players.get(0);
    }

    public Player getSecondPlayer() {
        return players.get(1);
    }

    public MatchResult getResultForFirst() {
        if (score[0] > score[1]) {
            return MatchResult.win;
        } else if (score[0] < score[1]) {
            return MatchResult.defeat;
        } else {
            return MatchResult.draw;
        }
    }

    @Override
    public long getMatchId() {
        return matchId;
    }

    @Override
    public JsonObject getInitMessage(Player recipient) {
        JsonObject data = new JsonObject();
        data.addProperty("type", "init");
        data.addProperty("playerIndex", indexOf(recipient));
        data.add("board", toJSON(gson));
        data.addProperty("size", MazeGameState.N);
        data.addProperty("stepsPerMove", MazeGameState.STEPS_PER_MOVE);
        data.addProperty("moveIndex", getMoveIndex());
        data.addProperty("player1Score", score[0]);
        data.addProperty("player2Score", score[1]);
        data.addProperty("skipMoveCnt", skipMoveCnt);
        data.add("player1", playerDescription(players.get(0)));
        data.add("player2", playerDescription(players.get(1)));
        return data;
    }

    private JsonArray toJSON(Gson gson) {
        JsonArray r = new JsonArray();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] != Unit.empty) {
                    r.add(gson.toJsonTree(new Cell(i, j, board[i][j])));
                }
            }
        }
        return r;
    }

    public int getMoveIndex() {
        return moveIndex;
    }

    public int indexOf(Player player) {
        return players.indexOf(player);
    }
}
