package games.maze;

public interface BoardGenerator {
    public Unit[][] generateBoard();
}
