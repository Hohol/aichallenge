package games.maze;

import games.maze.Position;
import games.maze.Unit;

import java.util.*;

import static games.maze.Unit.*;
import static java.lang.Math.*;
import static games.maze.MazeGameState.N;

public class MoveValidator {

    public static final int WALL = (int) 1e6;
    private static final int NOT_VISITED = -1;

    public boolean isValidMove(Unit[][] board, Unit whoMoves, List<Position> moves, int steps) {
        Position curPos = positionOf(board, whoMoves);

        int sumLen = 0;
        for (Position to : moves) {
            if(to.equals(curPos)) {
                return false;
            }
            if(!inside(to.x, to.y)) {
                return false;
            }
            Unit value = board[to.x][to.y];
            if(value != empty && value != gold && value != whoMoves) {
                return false;
            }
            int[][] dist = getStrictWallsForPlayer(board, whoMoves);
            dfs(curPos, dist);
            sumLen += dist[to.x][to.y];
            curPos = to;
        }

        return sumLen <= steps;
    }

    public boolean mayPlaceWall(Unit[][] board, Unit whoMoves, Position to) {
        if(!inside(to.x,to.y)) {
            return false;
        }
        if(board[to.x][to.y] != empty) {
            return false;
        }
        Unit whoNotMoves = whoMoves == player1 ? player2 : player1;
        int[][] dist = getStrictWallsForPlayer(board, whoNotMoves);
        dist[to.x][to.y] = WALL;
        dfs(positionOf(board, whoNotMoves), dist);
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if(dist[i][j] == NOT_VISITED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void dfs(Position start, int[][] dist) {
        Queue<Position> q = new ArrayDeque<>();
        q.add(start);
        dist[start.x][start.y] = 0;
        while (!q.isEmpty()) {
            Position p = q.remove();
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    if (abs(i + j) != 1) {
                        continue;
                    }
                    int toX = p.x + i, toY = p.y + j;
                    if (!inside(toX, toY)) {
                        continue;
                    }
                    if (dist[toX][toY] != NOT_VISITED) {
                        continue;
                    }
                    q.add(new Position(toX, toY));
                    dist[toX][toY] = dist[p.x][p.y] + 1;
                }
            }
        }
    }

    private boolean inside(int x, int y) {
        return x >= 0 && x < N && y >= 0 && y < N;
    }

    private int[][] getStrictWallsForPlayer(Unit[][] board, Unit unit) {
        int[][] r = new int[N][N];
        for (int[] row : r) {
            Arrays.fill(row, NOT_VISITED);
        }
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                Unit value = board[i][j];
                if (value == neutralWall ||
                        unit == player1 && value == wall2 ||
                        unit == player2 && value == wall1) {
                    r[i][j] = WALL;
                }
            }
        }
        return r;
    }

    Position positionOf(Unit[][] board, Unit unit) {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if(board[i][j] == unit) {
                    return new Position(i,j);
                }
            }
        }
        throw new RuntimeException();
    }
}
