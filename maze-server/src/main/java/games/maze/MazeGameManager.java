package games.maze;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import server.common.*;

import java.util.*;

public class MazeGameManager extends AbstractGameManager<MazeGameState> {

    private static final double K = 30;
    private final MatchRequestQueue matchRequests = new MatchRequestQueue();
    private final Random rnd = new Random();

    public synchronized void processMessage(Player player, String message, Endpoint endpoint) {
        JsonObject data = gson.fromJson(message, JsonObject.class);
        try {
            userToEndpoint.put(player, endpoint);
            String type = data.get("type").getAsString();
            switch (type) {
                case "game-request":
                    processGameRequest(player, data);
                    break;
                case "move":
                    makeMove(player, data);
                    break;
                case "ping":
                    //do nothing
                    break;
                default:
                    System.out.println("Unknown message type");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(message);
        }
    }

    @Override
    synchronized public void playerDisconnected(Player leaver) {
        Endpoint endpoint = userToEndpoint.get(leaver);
        if (endpoint == null) {
            return;
        }
        matchRequests.remove(leaver);
        MazeGameState gameState = activeGames.get(leaver);
        if (gameState != null) {
            activeGames.remove(leaver);
            MatchResult result;
            if (gameState.getFirstPlayer().equals(leaver)) {
                result = MatchResult.defeat;
            } else {
                result = MatchResult.win;
            }
            finishGame(gameState, result, leaver.getName() + " disconnected");
        }
    }

    private void finishGame(MazeGameState gameState, MatchResult resultForFirst, String description) {
        Player first = gameState.getFirstPlayer();
        Player second = gameState.getSecondPlayer();

        getEndpoint(first).stopListening(first, getGameName());
        getEndpoint(second).stopListening(second, getGameName());

        sendVerdict(first, resultForFirst, description);
        sendVerdict(second, resultForFirst.opposite(), description);

        userToEndpoint.remove(first);
        userToEndpoint.remove(second);

        activeGames.remove(first);
        activeGames.remove(second);

        DBConnector.getInstance().finishMatch(gameState.getMatchId());
        updateRating(first, second, resultForFirst);
    }

    private void updateRating(Player firstPlayer, Player secondPlayer, MatchResult resultForFirst) {
        DBConnector dbConnector = DBConnector.getInstance();
        String gameName = getGameName();
        Number r1 = dbConnector.getRating(gameName, firstPlayer.getId());
        Number r2 = dbConnector.getRating(gameName, secondPlayer.getId());

        boolean firstHasOldRating = r1 != null;
        boolean secondHasOldRating = r2 != null;
        final int defaultRating = 1000;
        if (!firstHasOldRating) {
            r1 = defaultRating;
        }
        if (!secondHasOldRating) {
            r2 = defaultRating;
        }

        double score1 = resultForFirst.score;
        double score2 = 1 - score1;

        double expectedScore1 = 1 / (1 + Math.pow(10, ((r2.doubleValue() - r1.doubleValue()) / 400)));
        double expectedScore2 = 1 - expectedScore1;
        double newR1 = r1.doubleValue() + K * (score1 - expectedScore1);
        double newR2 = r2.doubleValue() + K * (score2 - expectedScore2);

        dbConnector.updateRating(gameName, firstPlayer.getId(), firstHasOldRating, newR1);
        dbConnector.updateRating(gameName, secondPlayer.getId(), secondHasOldRating, newR2);
    }

    private void sendVerdict(Player user, MatchResult result, String description) {
        JsonObject data = new JsonObject();
        data.addProperty("type", "verdict");
        data.add("data", gson.toJsonTree(new Verdict(result, description)));
        send(user, data);
    }

    public void processGameRequest(Player player, JsonObject data) {
        if (activeGames.containsKey(player)) {
            sendInitialGameState(player, activeGames.get(player));
        } else {
            Boolean humanOnly = false;
            JsonElement ho = data.get("human_only");
            if(ho != null) {
                humanOnly = ho.getAsBoolean();
            }
            MatchRequest request = new MatchRequest(player, humanOnly);
            Player secondPlayer = matchRequests.findOpponentFor(request);
            if (secondPlayer == null) {
                matchRequests.add(request);
            } else {
                matchRequests.remove(player);
                matchRequests.remove(secondPlayer);
                startMatch(player, secondPlayer);
            }
        }
    }

    private void startMatch(Player firstPlayer, Player secondPlayer) {
        if (rnd.nextBoolean()) {
            Player buf = firstPlayer;
            firstPlayer = secondPlayer;
            secondPlayer = buf;
        }
        long matchId = DBConnector.getInstance().startMatch(getGameName(), firstPlayer.getId(), secondPlayer.getId());
        MazeGameState gameState = new MazeGameState(firstPlayer, secondPlayer, matchId);
        activeGames.put(firstPlayer, gameState);
        activeGames.put(secondPlayer, gameState);
        sendInitialGameState(firstPlayer, gameState);
        sendInitialGameState(secondPlayer, gameState);
    }

    public void makeMove(Player player, JsonObject data) {
        MazeGameState gameState = activeGames.get(player);
        if (gameState == null) {
            return;
        }
        if (gameState.whoMovesNow().equals(player)) {
            Player otherPlayer = gameState.whoNotMovesNow();
            JsonElement move = data.get("move");
            if (move.isJsonPrimitive() && move.getAsString().equals("skip")) {
                gameState.skipMove();
            } else if (move.isJsonArray()) {
                List<Position> steps = new ArrayList<>();
                for (JsonElement pos : move.getAsJsonArray()) {
                    steps.add(gson.fromJson(pos, Position.class));
                }
                if (gameState.validMove(steps)) {
                    gameState.makeMove(steps);
                } else {
                    gameState.print();
                    throw new RuntimeException("invalid move");
                }
            } else {
                Position to = gson.fromJson(move, Position.class);
                if (gameState.mayPlaceWall(to)) {
                    gameState.placeWall(to);
                } else {
                    gameState.print();
                    throw new RuntimeException("invalid move");
                }
            }
            JsonObject dataForWatchers = new JsonObject();
            dataForWatchers.addProperty("type", "move");
            dataForWatchers.add("move", move);
            send(otherPlayer, dataForWatchers);

            dataForWatchers.add("gameState", gameState.getInitMessage(null));
            broadcaster.sendToWatchers(gameState.getMatchId(), dataForWatchers);
            if (gameState.finished()) {
                finishGame(gameState, gameState.getResultForFirst(), "");
            }
            System.out.println("Active games: " + activeGames.size());
        }
    }

    @Override
    public String getGameName() {
        return "maze";
    }
}