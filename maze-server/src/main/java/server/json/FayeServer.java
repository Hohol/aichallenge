package server.json;

import java.util.*;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import server.common.*;
import org.cometd.bayeux.Channel;
import org.cometd.bayeux.Message;
import org.cometd.bayeux.client.ClientSessionChannel;
import org.cometd.client.BayeuxClient;
import org.cometd.client.transport.LongPollingTransport;
import org.eclipse.jetty.client.HttpClient;

public class FayeServer extends JsonEndpoint implements Runnable, WatcherBroadcaster {

    private static final long PING_INTERVAL = 10000;
    private volatile BayeuxClient client;

    private final Map<String, GameManager> gameToManager = new HashMap<>();
    private final Map<Pair<Player, String>, Long> userGameToTimestamp =
            Collections.synchronizedMap(new HashMap<Pair<Player, String>, Long>());
    private final String defaultURL;

    public FayeServer(List<GameManager> gameManagers, String fayeUrl) {
        for (GameManager gameManager : gameManagers) {
            gameToManager.put(gameManager.getGameName(), gameManager);
            gameManager.setBroadcaster(this); //todo =(
        }
        this.defaultURL = fayeUrl;
    }
    //private final MembersListener membersListener = new MembersListener();

    @Override
    public void run() {
        System.err.printf("Enter Bayeux Server URL [%s]: ", defaultURL);

        HttpClient httpClient = new HttpClient();
        try {
            httpClient.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        client = new BayeuxClient(defaultURL, new LongPollingTransport(null, httpClient));
        client.getChannel(Channel.META_HANDSHAKE).addListener(new InitializerListener());
        //client.getChannel(Channel.META_CONNECT).addListener(new ConnectionListener());

        client.handshake();
        boolean success = client.waitFor(1000, BayeuxClient.State.CONNECTED);
        if (!success) {
            System.err.printf("Could not handshake with server at %s%n", defaultURL);
        }

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                for (Iterator<Map.Entry<Pair<Player, String>, Long>> it = userGameToTimestamp.entrySet().iterator(); it.hasNext(); ) {
                    Map.Entry<Pair<Player, String>, Long> entry = it.next();
                    Player player = entry.getKey().first;
                    String game = entry.getKey().second;
                    long timestamp = entry.getValue();
                    long curTime = System.currentTimeMillis();
                    /*System.out.println(user);
                    System.out.println(game);
                    System.out.println(curTime - timestamp);/**/
                    if (curTime - timestamp > PING_INTERVAL) {
                        System.out.println(player + " disconnected");
                        gameToManager.get(game).playerDisconnected(player);
                        it.remove();
                    }
                }
            }
        };
        new Timer(true).schedule(timerTask, PING_INTERVAL, PING_INTERVAL);
    }

    private void initialize() {
        client.batch(new Runnable() {
            public void run() {
                for (String gameName : gameToManager.keySet()) {
                    ClientSessionChannel chatChannel = client.getChannel("/" + gameName);
                    chatChannel.subscribe(new FayeMessageListener());
                }

                client.getChannel("/request-state").subscribe(new RequestGameStateListener());

                /*ClientSessionChannel membersChannel = client.getChannel("/members/demo");
                membersChannel.subscribe(membersListener);/**/
            }
        });
    }

    @Override
    public void send(Player player, JsonElement message) {
        message = gson.toJsonTree(message);

        System.out.println("sending to " + player + ":");
        System.out.println(message);
        System.out.println();

        String channelName = player.getToken();
        publish(channelName, message);
    }

    private void publish(String channelName, JsonElement message) {
        Object preparedMessage = toMapsAndPrimitives(message);
        client.getChannel("/" + channelName).publish(preparedMessage, new ClientSessionChannel.MessageListener() {
            @Override
            public void onMessage(ClientSessionChannel channel, Message message) {
                System.out.println(message);
            }
        });
    }

    @Override
    public void stopListening(Player user, String game) {
        // it seems nothing is needed here now
    }

    @Override
    public void sendToWatchers(long matchId, JsonElement message) {
        String channelName = "watch-" + matchId;
        message = gson.toJsonTree(message);

        System.out.println("sending to watchers, channel = " + channelName);
        System.out.println(message);
        System.out.println();

        publish(channelName, message);
    }


    private class InitializerListener implements ClientSessionChannel.MessageListener {
        public void onMessage(ClientSessionChannel channel, Message message) {
            if (message.isSuccessful()) {
                initialize();
            } else {
                throw new RuntimeException("connection failed");
            }
        }
    }

    /*private void connectionEstablished() {
        System.err.printf("system: Connection to Server Opened%n");
    }

    private void connectionClosed() {
        System.err.printf("system: Connection to Server Closed%n");
    }

    private void connectionBroken() {
        System.err.printf("system: Connection to Server Broken%n");
    }
    private class ConnectionListener implements ClientSessionChannel.MessageListener {
        private boolean wasConnected;
        private boolean connected;

        public void onMessage(ClientSessionChannel channel, Message message) {
            if (client.isDisconnected()) {
                connected = false;
                connectionClosed();
                return;
            }

            wasConnected = connected;
            connected = message.isSuccessful();
            if (!wasConnected && connected) {
                connectionEstablished();
            } else if (wasConnected && !connected) {
                connectionBroken();
            }
        }
    }/**/

    private class FayeMessageListener implements ClientSessionChannel.MessageListener {
        public void onMessage(ClientSessionChannel channel, Message message) {
            try {
                JsonObject jo = gson.toJsonTree(message.getDataAsMap()).getAsJsonObject();
                String humanToken = jo.get("human_token").getAsString();
                Player player = DBConnector.getInstance().getHuman(humanToken);
                String game = channel.toString().substring(1);

                userGameToTimestamp.put(new Pair<>(player, game), System.currentTimeMillis());

                JsonElement data = jo.get("data");
                if (data == null) {
                    return;
                }

                System.out.println("Faye server received:");
                System.out.println(jo);
                System.out.println();

                gameToManager.get(game).processMessage(player, data.toString(), FayeServer.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class RequestGameStateListener implements ClientSessionChannel.MessageListener {
        public void onMessage(ClientSessionChannel channel, Message message) {
            try {
                JsonObject jo = gson.toJsonTree(message.getDataAsMap()).getAsJsonObject();
                System.out.println("new watch-request:");
                System.out.println(jo);
                System.out.println();

                String watcherToken = jo.get("watcherToken").getAsString();
                String game = jo.get("game").getAsString();
                long matchId = jo.get("matchId").getAsLong();

                JsonElement gameState = gameToManager.get(game).getInitMessageForWatcher(matchId);
                publish(watcherToken, gameState);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*private class MembersListener implements ClientSessionChannel.MessageListener {
        public void onMessage(ClientSessionChannel channel, Message message) {
            Object data = message.getData();
            Object[] members = data instanceof List ? ((List) data).toArray() : (Object[]) data;
            System.err.printf("Members: %s%n", Arrays.asList(members));
        }
    }/**/
}