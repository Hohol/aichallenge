package server.json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import server.common.DBConnector;
import server.common.GameManager;
import server.common.Player;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class SocketServer implements Runnable {

    private final Map<String, GameManager> gameToManager = new HashMap<>();
    public SocketServer(List<GameManager> gameManagers) {
        for (GameManager gameManager : gameManagers) {
            gameToManager.put(gameManager.getGameName(), gameManager);
        }
    }

    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(9293);
            while(true) {
                Socket socket = serverSocket.accept();
                new Thread(new MiniEndpoint(socket, gameToManager)).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class MiniEndpoint extends JsonEndpoint implements Runnable {
        private final Socket socket;
        private final Map<String, GameManager> gameToManager;
        private PrintWriter out;
        private Scanner in;
        private boolean stopListening;
        public MiniEndpoint(Socket socket, Map<String, GameManager> gameToManager) {
            this.socket = socket;
            this.gameToManager = gameToManager;
        }

        @Override
        public void send(Player user, JsonElement message) {
            out.println(message);
        }

        @Override
        public void stopListening(Player user, String game) {
            stopListening = true;
            try {
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {

            GameManager gameManager;
            Player player;

            try {
                out = new PrintWriter(socket.getOutputStream(), true);
                in = new Scanner(socket.getInputStream());
                String message = readMessage();
                JsonObject jo = gson.fromJson(message, JsonObject.class);
                String game = jo.get("game").getAsString();
                player = DBConnector.getInstance().getBot(jo.get("bot_token").getAsString());
                System.out.println(player);
                JsonObject data = jo.get("data").getAsJsonObject();
                gameManager = gameToManager.get(game);
                gameManager.processMessage(player, data.toString(), this);
            } catch (JsonSyntaxException e) {
                out.println("Incorrect JSON syntax");
                return;
            } catch (IOException e) {
                e.printStackTrace();
                return;
            } catch (Exception e) {
                out.println("Something went wrong. Probably, some expected field is missing.");
                return;
            }

            try {
                while(!stopListening) {
                    String message = readMessage();
                    gameManager.processMessage(player, message, this);
                }
            } catch (JsonSyntaxException e) {
                out.println("Incorrect JSON syntax");
            } catch (IOException e) {
                if(!stopListening) {
                    gameManager.playerDisconnected(player);
                }
            } catch (Exception e) {
                out.println("Something went wrong. Probably, some expected field is missing.");
            }
        }

        private String readMessage() throws IOException {
            try {
                String msg = in.nextLine();
                System.out.println("Socket server received:");
                System.out.println(msg);
                System.out.println();
                return msg;
            } catch (Exception e) {
                throw new IOException(e);
            }
        }
    }
}
