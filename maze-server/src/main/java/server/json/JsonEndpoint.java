package server.json;

import com.google.gson.*;
import server.common.Endpoint;

public abstract class JsonEndpoint implements Endpoint {

    protected final Gson gson = new Gson();
    private final Gson gsonNatural; {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Object.class, new NaturalDeserializer());
        gsonNatural = gsonBuilder.create();
    }
    protected Object toMapsAndPrimitives(JsonElement data) {
        return gsonNatural.fromJson(data, Object.class);
    }
}
