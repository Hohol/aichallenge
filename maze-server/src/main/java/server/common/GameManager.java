package server.common;

import com.google.gson.JsonElement;

public interface GameManager {
    String getGameName();
    void processMessage(Player leaver, String message, Endpoint endpoint);
    void playerDisconnected(Player username);
    void setBroadcaster(WatcherBroadcaster broadcaster); //todo =(
    JsonElement getInitMessageForWatcher(long matchId);
}
