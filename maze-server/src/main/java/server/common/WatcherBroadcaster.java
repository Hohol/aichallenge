package server.common;

import com.google.gson.JsonElement;

public interface WatcherBroadcaster {
    void sendToWatchers(long matchId, JsonElement message);
}
