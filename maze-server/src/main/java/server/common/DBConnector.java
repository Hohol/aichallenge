package server.common;

import java.sql.*;

public class DBConnector {

    private final String dbPath;

    private static DBConnector instance;
    private DBConnector(String dbPath) {
        this.dbPath = dbPath;
    }

    public static void init(String dbPath) {
        instance = new DBConnector(dbPath);
    }

    public static DBConnector getInstance() {
        return instance;
    }

    public static void main(String[] args) {
        //DBConnector connector = new DBConnector(dbPath);
        //connector.updateRating("2048", 11, true, 6000);
        //System.out.println(connector.getRating("2048", 11));
        //connector.startMatch("maze", 1, 2);
        //connector.finishMatch(3);
    }/**/

    public Player getHuman(String humanToken) {
        return getPlayer(humanToken, PlayerType.human, "f");
    }

    public Player getBot(String botToken) {
        return getPlayer(botToken, PlayerType.bot, "t");
    }

    private Player getPlayer(String token, PlayerType type, String isBotSign) { //todo store token with player
        try (Connection connection = getConnection()) {
            String query = "select username, id from users where " + type + "_token = ?";
            PreparedStatement p = connection.prepareStatement(query);
            p.setString(1, token);
            ResultSet rs = p.executeQuery();
            String username = rs.getString(1);
            if(username == null) {
                throw new RuntimeException("Unknown bot token");
            }
            long userId = rs.getLong(2);
            query = "select id from players where user_id = ? and is_bot = ?";
            p = connection.prepareStatement(query);
            p.setLong(1, userId);
            p.setString(2, isBotSign);
            rs = p.executeQuery();
            long playerId = rs.getLong(1);
            return new Player(username, type, token, playerId);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void updateRating(String game, long playerId, boolean hasOldRating, Number newRating) {
        try (Connection connection = getConnection()) {
            long currentTime = System.currentTimeMillis();
            if(hasOldRating) {
                String query = "update ratings " +
                        "set value = ?, updated_at = ? " +
                        "where game = ? and player_id = ?";
                PreparedStatement p = getConnection().prepareStatement(query);
                p.setDouble(1, newRating.doubleValue());
                p.setLong(2, currentTime);
                p.setString(3, game);
                p.setLong(4, playerId);
                p.executeUpdate();
            } else {
                String query = "insert into ratings (value, game, player_id, created_at, updated_at) " +
                        "values (?, ?, ?, ?, ?)";
                PreparedStatement p = connection.prepareStatement(query);
                p.setDouble(1, newRating.doubleValue());
                p.setString(2, game);
                p.setLong(3, playerId);
                p.setLong(4, currentTime);
                p.setLong(5, currentTime);
                p.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public Number getRating(String game, long playerId) {
        try (Connection connection = getConnection()) {
            String query = "select value from ratings where game = ? and player_id = ?";
            PreparedStatement p = connection.prepareStatement(query);
            p.setString(1, game);
            p.setLong(2, playerId);
            ResultSet rs = p.executeQuery();
            return (Number) rs.getObject(1);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private Connection getConnection() throws SQLException{
        try {
            Class.forName("org.sqlite.JDBC");
            return DriverManager.getConnection(dbPath);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public long startMatch(String game, long ...playerIds) {
        long currentTime = System.currentTimeMillis();
        try (Connection connection = getConnection()) {
            String query = "insert into matches (game, finished, created_at, updated_at) values (?, 'f', ?, ?)";
            PreparedStatement p = connection.prepareStatement(query);
            p.setString(1, game);
            p.setLong(2, currentTime);
            p.setLong(3, currentTime);
            p.executeUpdate();


            long matchId = p.getGeneratedKeys().getLong(1);
            for (long playerId : playerIds) {
                query = "insert into matches_players (match_id, player_id) values (?, ?)";
                p = connection.prepareStatement(query);
                p.setLong(1, matchId);
                p.setLong(2, playerId);
                p.executeUpdate();
            }
            return matchId;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void finishMatch(long matchId) {
        try (Connection connection = getConnection()) {
            String query = "update matches set finished = 't' where id = ?";
            PreparedStatement p = connection.prepareStatement(query);
            p.setLong(1, matchId);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void clearMatches() {
        try (Connection connection = getConnection()) {
            String query = "delete from matches";
            PreparedStatement p = connection.prepareStatement(query);
            p.executeUpdate();

            query = "delete from matches_players";
            p = connection.prepareStatement(query);
            p.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
