package server.common;

public enum MatchResult {
    win(1), defeat(0), draw(0.5);
    public final double score;

    MatchResult(double score) {
        this.score = score;
    }

    public MatchResult opposite() {
        switch (this) {
            case win:
                return defeat;
            case defeat:
                return win;
            case draw:
                return draw;
        }
        throw new RuntimeException();
    }
}
