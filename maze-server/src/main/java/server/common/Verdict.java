package server.common;

public class Verdict {
    private MatchResult result;
    private String description;

    public Verdict() {}

    public Verdict(MatchResult result, String description) {
        this.result = result;
        this.description = description;
    }
}
