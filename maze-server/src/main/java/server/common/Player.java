package server.common;

public class Player {
    private final String username;
    private final String token;
    private final long id;
    private final PlayerType type;

    public Player(String username, PlayerType type, String token, long id) {
        this.username = username;
        this.token = token;
        this.id = id;
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public PlayerType getType() {
        return type;
    }

    public String getToken() {
        return token;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return id == player.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Player{" +
                "username='" + username + '\'' +
                ", token='" + token + '\'' +
                ", id=" + id +
                '}';
    }

    public String getName() {
        return username; //todo isbot
    }
}
