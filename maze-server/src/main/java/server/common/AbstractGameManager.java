package server.common;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractGameManager<M extends Match> implements GameManager{
    protected final Gson gson = new Gson();
    protected final Map<Player, Endpoint> userToEndpoint = new HashMap<>();
    protected final Map<Player, M> activeGames = new HashMap<>();
    protected WatcherBroadcaster broadcaster;

    public void setBroadcaster(WatcherBroadcaster broadcaster) {
        this.broadcaster = broadcaster; //todo =(
    }

    protected Endpoint getEndpoint(Player user) {
        return userToEndpoint.get(user);
    }

    protected void send(Player user, JsonElement message) {
        getEndpoint(user).send(user, message);
    }

    protected void sendInitialGameState(Player player, M gameState) {
        send(player, gameState.getInitMessage(player));
    }

    protected M findMatch(long matchId) {
        for (M gameState : activeGames.values()) { // todo =(
            if(gameState.getMatchId() == matchId) {
                return gameState;
            }
        }
        return null;
    }

    @Override
    synchronized public JsonElement getInitMessageForWatcher(long matchId) {
        M match = findMatch(matchId);
        if(match == null) {
            JsonObject jo = new JsonObject();
            jo.addProperty("type", "error");
            jo.addProperty("description", "Match finished");
            return jo;
        } else {
            return match.getInitMessage(null);
        }
    }

    protected void sendError(Player player, String message) {
        JsonObject jo = new JsonObject();
        jo.addProperty("type", "error");
        jo.addProperty("message", message);
        send(player, jo);
    }
}
