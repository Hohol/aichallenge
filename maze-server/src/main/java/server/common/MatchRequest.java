package server.common;

public class MatchRequest {
    private final Player player;
    private final boolean humanOnly;

    public MatchRequest(Player player, boolean humanOnly) {
        this.player = player;
        this.humanOnly = humanOnly;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean fits(MatchRequest m) {
        if(player.equals(m.getPlayer())) {
            return false;
        }
        return fitsOneSide(m) && m.fitsOneSide(this);
    }

    private boolean fitsOneSide(MatchRequest m) {
        if(humanOnly && m.getPlayer().getType() == PlayerType.bot) {
            return false;
        }
        return true;
    }
}
