package server.common;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public abstract class AbstractMatch implements Match {
    protected final long matchId;
    protected final Gson gson = new Gson();

    public AbstractMatch(long matchId) {
        this.matchId = matchId;
    }

    protected JsonObject playerDescription(Player player) {
        JsonObject jo = new JsonObject();
        jo.addProperty("username", player.getUsername());
        jo.addProperty("type", player.getType().toString());
        return jo;
    }

    public long getMatchId() {
        return matchId;
    }
}
