package server.common;

import java.util.ArrayDeque;
import java.util.Queue;

public class MatchRequestQueue {
    Queue<MatchRequest> q = new ArrayDeque<>();

    public void remove(Player player) {
        for (MatchRequest matchRequest : q) {
            if(matchRequest.getPlayer().equals(player)) {
                q.remove(matchRequest);
                return;
            }
        }
    }

    public Player findOpponentFor(MatchRequest newRequest) {
        for (MatchRequest oldRequest : q) {
            if(oldRequest.fits(newRequest)) {
                q.remove(oldRequest);
                return oldRequest.getPlayer();
            }
        }
        return null;
    }

    public void add(MatchRequest newRequest) {
        for (MatchRequest oldRequest : q) {
            if(newRequest.getPlayer().equals(oldRequest.getPlayer())) {
                q.remove(oldRequest);
                break;
            }
        }
        q.add(newRequest);
    }
}
