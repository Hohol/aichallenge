package server.common;

import com.google.gson.JsonObject;

public interface Match {
    long getMatchId();

    /**
     *
     * @param recipient is null for watchers
     * @return Initial game state message. It may be different for different recipients.
     */

    JsonObject getInitMessage(Player recipient);
}
