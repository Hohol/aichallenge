package server.common;

import com.google.gson.JsonElement;

public interface Endpoint {
    void send(Player user, JsonElement message);
    void stopListening(Player user, String game); //todo particular match
}
