package server.common;

import games.game2048.Game2048Manager;
import server.json.FayeServer;
import server.json.SocketServer;
import games.maze.MazeGameManager;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {
        String dbPath = args[0];
        String fayePath = args[1];

        DBConnector.init(dbPath);
        DBConnector.getInstance().clearMatches();
        List<GameManager> gameManagers = new ArrayList<>();
        gameManagers.add(new MazeGameManager());
        gameManagers.add(new Game2048Manager());

        FayeServer fayeServer = new FayeServer(gameManagers, fayePath);
        SocketServer socketServer = new SocketServer(gameManagers);
        new Thread(fayeServer).start();
        new Thread(socketServer).start();
    }
}